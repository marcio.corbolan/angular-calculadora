/**
 * Serviço responsável por executar as operações de
 * calculo.
 *
 * @author Márcio O. Corbolan<marciocorbolan@hotmail.com>
 * @since 1.0.0
 */

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  /* Constantes utilizadas nas operações de cálculo */
  static readonly SOMAR: string = '+';
  static readonly SUBTRAIR: string = '-';
  static readonly DIVIDIR: string = '/';
  static readonly MULTIPLICAR: string = '*';
  static readonly OPERACOES: Array<string> = [
    CalculadoraService.SOMAR,
    CalculadoraService.SUBTRAIR,
    CalculadoraService.DIVIDIR,
    CalculadoraService.MULTIPLICAR
  ];

  constructor() { }

  /**
   * Método responsável por efetuar uma operação matemática entre
   * dois números.
   * Suporta para as operações de somar, subtrair, dividir,
   * e multiplicar.
   *
   * @param num1 number
   * @param num2 number
   * @param operacao string Operação a ser executada
   * @return number Resultado da operação
   */
  calcular(num1: number, num2: number, operacao: string): number {
    let resultado: number;

    switch (operacao) {
      case CalculadoraService.SOMAR:
        resultado = num1 + num2;
        break;
      case CalculadoraService.SUBTRAIR:
        resultado = num1 - num2;
        break;
      case CalculadoraService.DIVIDIR:
        resultado = num1 / num2;
        break;
      case CalculadoraService.MULTIPLICAR:
        resultado = num1 * num2;
        break;
      default:
        resultado = 0;
    }

    return resultado;
  }
}
