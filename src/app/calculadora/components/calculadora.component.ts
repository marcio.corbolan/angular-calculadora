import { Component, OnInit } from '@angular/core';

import { CalculadoraService } from '../services';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.css']
})
export class CalculadoraComponent implements OnInit {

  private numero1: string;
  private numero2: string;
  private resultado: number;
  private operacao: string;

  constructor(private calculadoraService: CalculadoraService) { }

  ngOnInit() {
    this.limpar();
  }

  /**
   * Inicializa/reseta todas as variáveis utilizadas
   * no calculo.
   *
   * @return void
   */
  limpar(): void {
    this.numero1 = '0';
    this.numero2 = null;
    this.resultado = null;
    this.operacao = null;
  }

  /**
   * Adiciona o número selecionado no cálculo.
   *
   * @param string numero
   * @return void
   */
  adicionarNumero(numero: string): void {
    if (this.operacao === null) {
      this.numero1 = this.concatenarNumero(this.numero1, numero);
    } else {
      this.numero2 = this.concatenarNumero(this.numero2, numero);
    }
  }

  /**
   * Concatena o valor selecionado com o(s) valor(es)
   * previamente adicionado(s).
   * Efetua o tratamento do separador decimal.
   *
   * @param string numAtual
   * @param string numConcat
   * @return string
   */
  concatenarNumero(numAtual: string, numConcat: string): string {
    // Caso contenha apenas '0' ou null, o valor é reiniciado.
    if (numAtual === '0' || numAtual === null) {
      numAtual = '';
    }

    // Caso o primeiro dígito seja '.', o '0' é concatenado antes do ponto.
    if (numConcat === '.' && numAtual === '') {
      return '0.';
    }

    // Caso o '.' já tenha sido adicionado, ao tentar noveamente, este será ignorado.
    if (numConcat === '.' && numAtual.indexOf('.') > -1) {
      return numAtual;
    }

    return numAtual + numConcat;
  }

  /**
   * Executa lógica quando um operador for selecionado.
   * Caso uma operação já tenha sido selecionada, a
   * operação anterior é executa primeiro e a nova
   * operação é definida.
   *
   * @param string operacao
   * @return void
   */
  definirOperacao(operacao: string): void {
    // A operação só será atribuida caso não tenha ocorrido posteriormente.
    if (this.operacao === null) {
      this.operacao = operacao;
      return;
    }

    if ((this.numero2 !== null) && (this.operacao !== null)) {
      this.resultado = this.calculadoraService.calcular(
        parseFloat(this.numero1),
        parseFloat(this.numero2),
        this.operacao
      );

      this.operacao = operacao;
      this.numero1 = this.resultado.toString();
      this.numero2 = null;
      this.resultado = null;
    }
  }

  /**
   * Efetua o cálculo de uma operação.
   *
   * @return void
   */
  calcular(): void {
    /**
     * O calculo só será efetuado caso os dois números
     * tenham sido informados. Não é preciso validar a
     * operação, pois o segundo número só foi atribuido
     * devido a operação ter sido previamente atribuida.
     */
    if (this.numero2 === null) {
      return;
    }

    this.resultado = this.calculadoraService.calcular(
      parseFloat(this.numero1),
      parseFloat(this.numero2),
      this.operacao
    );
  }

  /**
   * Exibe os dados na tela da calculadora.
   *
   * @return string
   */
  get display(): string {
    if (this.resultado !== null) {
      return this.resultado.toString();
    }

    if (this.numero2 !== null) {
      return this.numero2;
    }

    return this.numero1;
  }
}
