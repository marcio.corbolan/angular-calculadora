# Calculadora em Angular

### Especificações

- **Angular**: 8.0.0

### Instruções

- **Deploy**
```console
- $ng build --prod
```

- **Dev**
```console
- $ng serve
```
> Navigate to `http://localhost:4200/`.

- **Teste unitário**
```console
- $ng test
```
> Executa os testes unitários.

- **Teste de fluxo**
```console
- $ng e2e
```
> Executa os testes de fluxo.
