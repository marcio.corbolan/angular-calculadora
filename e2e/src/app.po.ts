import { browser, by, element } from 'protractor';

import { CalculadoraService } from './../../src/app/calculadora/services';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  adicionarNumero(num: number) {
    const btnValor = element(by.css('#btn' + num));
    const display = element(by.css('#display'));

    btnValor.click();

    return display.getAttribute('value') as Promise<string>;
  }

  adicionarDecimal(num: number) {
    const btnValor = element(by.css('#btn' + num));
    const btnDecimal = element(by.css('#btnDecimal'));
    const display = element(by.css('#display'));

    btnDecimal.click();
    btnValor.click();

    return display.getAttribute('value') as Promise<string>;
  }

  limpar() {
    const btnValor = element(by.css('#btn1'));
    const btnLimpar = element(by.css('#btnLimpar'));
    const display = element(by.css('#display'));

    btnLimpar.click();

    return display.getAttribute('value') as Promise<string>;
  }

  efetuarCalculo(num1: number, num2: number, operacao: string) {
    switch (operacao) {
      case CalculadoraService.SOMAR:
        operacao = 'Somar';
        break;
      case CalculadoraService.SUBTRAIR:
        operacao = 'Subtrair';
        break;
      case CalculadoraService.DIVIDIR:
        operacao = 'Dividir';
        break;
      case CalculadoraService.MULTIPLICAR:
        operacao = 'Multiplicar';
        break;
      default:
        operacao = '';
    }

    const btnValor1 = element(by.css('#btn' + num1));
    const btnOperacao = element(by.css('#btn' + operacao));
    const btnValor2 = element(by.css('#btn' + num2));
    const btnCalcular = element(by.css('#btnCalcular'));
    const display = element(by.css('#display'));

    btnValor1.click();
    btnOperacao.click();
    btnValor2.click();
    btnCalcular.click();

    return display.getAttribute('value') as Promise<string>;
  }
}
