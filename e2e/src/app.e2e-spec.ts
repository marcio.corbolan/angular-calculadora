import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

import { CalculadoraService } from './../../src/app/calculadora/services';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Deve garantir que ao apertar o botão 0 exiba 0 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(0)).toEqual('0');
  });

  it('Deve garantir que ao apertar o botão 1 exiba 1 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(1)).toEqual('1');
  });

  it('Deve garantir que ao apertar o botão 2 exiba 2 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(2)).toEqual('2');
  });

  it('Deve garantir que ao apertar o botão 3 exiba 3 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(3)).toEqual('3');
  });

  it('Deve garantir que ao apertar o botão 4 exiba 4 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(4)).toEqual('4');
  });

  it('Deve garantir que ao apertar o botão 5 exiba 5 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(5)).toEqual('5');
  });

  it('Deve garantir que ao apertar o botão 6 exiba 6 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(6)).toEqual('6');
  });

  it('Deve garantir que ao apertar o botão 7 exiba 7 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(7)).toEqual('7');
  });

  it('Deve garantir que ao apertar o botão 8 exiba 8 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(8)).toEqual('8');
  });

  it('Deve garantir que ao apertar o botão 9 exiba 9 no display', () => {
    page.navigateTo();
    expect(page.adicionarNumero(9)).toEqual('9');
  });

  it('Deve garantir que ao apertar o "." exiba um número decimal no display', () => {
    page.navigateTo();
    expect(page.adicionarDecimal(1)).toEqual('0.' + 1);
  });

  it('Deve garantir que o display seja limpo', () => {
    page.navigateTo();
    expect(page.limpar()).toEqual('0');
  });

  it('Deve garantir que 1 + 4 = 5', () => {
    page.navigateTo();
    expect(page.efetuarCalculo(1, 4, CalculadoraService.SOMAR)).toEqual('5');
  });

  it('Deve garantir que 1 - 4 = -3', () => {
    page.navigateTo();
    expect(page.efetuarCalculo(1, 4, CalculadoraService.SUBTRAIR)).toEqual('-3');
  });

  it('Deve garantir que 1 / 4 = 0.25', () => {
    page.navigateTo();
    expect(page.efetuarCalculo(1, 4, CalculadoraService.DIVIDIR)).toEqual('0.25');
  });

  it('Deve garantir que 1 * 4 = 4', () => {
    page.navigateTo();
    expect(page.efetuarCalculo(1, 4, CalculadoraService.MULTIPLICAR)).toEqual('4');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
