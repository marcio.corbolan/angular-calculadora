import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { CalculadoraComponent } from '../components';
import { CalculadoraService } from '../services';

describe('CalculadoraComponent', () => {
  let component: CalculadoraComponent;
  let fixture: ComponentFixture<CalculadoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalculadoraComponent],
      providers: [
        CalculadoraService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculadoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Deve garantir que 1 + 4 = 5', () => {
    const btn1 = fixture.debugElement.query(By.css('#btn1'));
    const btnSomar = fixture.debugElement.query(By.css('#btnSomar'));
    const btn4 = fixture.debugElement.query(By.css('#btn4'));
    const btnCalcular = fixture.debugElement.query(By.css('#btnCalcular'));
    const display = fixture.debugElement.query(By.css('#display'));

    btn1.triggerEventHandler('click', null);
    fixture.detectChanges();

    btnSomar.triggerEventHandler('click', null);
    fixture.detectChanges();

    btn4.triggerEventHandler('click', null);
    fixture.detectChanges();

    btnCalcular.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(display.nativeElement.value).toEqual('5');
  });

  it('Deve garantir que 1 - 4 = -3', () => {
    const btn1 = fixture.debugElement.query(By.css('#btn1'));
    const btnSubtrair = fixture.debugElement.query(By.css('#btnSubtrair'));
    const btn4 = fixture.debugElement.query(By.css('#btn4'));
    const btnCalcular = fixture.debugElement.query(By.css('#btnCalcular'));
    const display = fixture.debugElement.query(By.css('#display'));

    btn1.triggerEventHandler('click', null);
    fixture.detectChanges();

    btnSubtrair.triggerEventHandler('click', null);
    fixture.detectChanges();

    btn4.triggerEventHandler('click', null);
    fixture.detectChanges();

    btnCalcular.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(display.nativeElement.value).toEqual('-3');
  });

  it('Deve garantir que 1 / 4 = 0.25', () => {
    const btn1 = fixture.debugElement.query(By.css('#btn1'));
    const btnDividir = fixture.debugElement.query(By.css('#btnDividir'));
    const btn4 = fixture.debugElement.query(By.css('#btn4'));
    const btnCalcular = fixture.debugElement.query(By.css('#btnCalcular'));
    const display = fixture.debugElement.query(By.css('#display'));

    btn1.triggerEventHandler('click', null);
    fixture.detectChanges();

    btnDividir.triggerEventHandler('click', null);
    fixture.detectChanges();

    btn4.triggerEventHandler('click', null);
    fixture.detectChanges();

    btnCalcular.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(display.nativeElement.value).toEqual('0.25');
  });

  it('Deve garantir que 1 * 4 = 4', () => {
    const btn1 = fixture.debugElement.query(By.css('#btn1'));
    const btnMultiplicar = fixture.debugElement.query(By.css('#btnMultiplicar'));
    const btn4 = fixture.debugElement.query(By.css('#btn4'));
    const btnCalcular = fixture.debugElement.query(By.css('#btnCalcular'));
    const display = fixture.debugElement.query(By.css('#display'));

    btn1.triggerEventHandler('click', null);
    fixture.detectChanges();

    btnMultiplicar.triggerEventHandler('click', null);
    fixture.detectChanges();

    btn4.triggerEventHandler('click', null);
    fixture.detectChanges();

    btnCalcular.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(display.nativeElement.value).toEqual('4');
  });
});
